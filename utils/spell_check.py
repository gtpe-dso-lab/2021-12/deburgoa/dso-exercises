from pathlib import Path
import tempfile
import subprocess
import sys
import demoji
import requests

LOCALE = "en_US"
DICTIONARY = 'dictionary.txt'

DICT_FILES = {
    'en_US.aff':  'https://cgit.freedesktop.org/libreoffice/dictionaries/plain/en/en_US.aff?id=a4473e06b56bfe35187e302754f6baaa8d75e54f',
    'en_US.dic': 'https://cgit.freedesktop.org/libreoffice/dictionaries/plain/en/en_US.dic?id=a4473e06b56bfe35187e302754f6baaa8d75e54f'
}

def get_dictionaries():
    for f, url in DICT_FILES.items():
        if not (Path(f).is_file() and Path(f).stat().st_size > 1000):
            r = requests.get(url, allow_redirects=True)
            open(f, 'wb').write(r.content)

def update_local_dictionary(tmp_d):
    # add image names to the local dictionary
    d = Path(DICTIONARY).read_text()
    media = Path('slides-source/media/').glob('**/*.*')
    media_names = [m.name for m in media]
    d_list = d.split('\n')
    d_list.extend(media_names)
    new_d = '\n'.join(d_list)
    Path(tmp_d.name).write_text(new_d)

def remove_emoji(s):
    if not demoji.last_downloaded_timestamp():
        demoji.download_codes()
    return demoji.replace(s, '')

def lint_spelling(files):
    """ hunspell -l -p <local dictionary> -d en_US <file> | uniq | sort

        ignore emojis
    """
    with tempfile.NamedTemporaryFile(suffix=".txt") as local_dictionary:
        get_dictionaries()
        update_local_dictionary(local_dictionary)

        misspellings = {}

        for f in files:          
            with tempfile.NamedTemporaryFile(suffix=".md") as tmp:
                f_str = f.read_text()

                f_str = remove_emoji(f_str)

                Path(tmp.name).write_text(f_str)
                args = ["hunspell", "-l", "-p", local_dictionary.name, "-d", LOCALE, tmp.name]
                raw = subprocess.check_output([*map(str, args)]).decode("utf-8").strip()
                words = sorted(set(raw.splitlines()))

            if words:
                misspellings[f] = sorted(words)

    if misspellings:
        misspells = sorted(set(sum(misspellings.values(), [])))

        print("\nMisspelled words in:")
        for key, value in misspellings.items():
            print(str(key), "\n", "\n\t".join(sorted(value)))
        print("\nAll Misspelled Words:")
        print()
        print("\n".join(misspells))
        return len(misspells)
    return 0

md_files = Path('.').glob('*.md')

# exit with exit code equal to misspellings count
# this causes ci to fail if there are any misspelled words
sys.exit(lint_spelling(md_files))
